from setuptools import setup, find_packages

with open('requirements.txt') as file:
    reqs = file.read().splitlines()

setup(
    name='config-creator',
    version='1.0.0',
    scripts=['scripts/ConfigCreator'],
    packages=find_packages(),
    description='Configuration Creator for the DES Tool',
    author='Brent Barbachem',
    author_email='barbacbd@dukes.jmu.edu',
    license='Proprietary',
    include_package_data=True,
    install_requires=reqs,
    dependency_links=[
        'https://pypi.org/simple/'
    ],
    zip_safe=False
)
