# ConfigCreator


## Description

The project was intended to create configuration or simulation files that can be used for either version of the DESTool project.
The original version of the project required a rather specific text file format, and it will be easier for a user to use this project
in order to execute that project. The newer version of the DESTool uses a json file, which should be easy to output using the Qt
pipeline. 

## Supported File Types

- JSON - These files are used for the newer version of the DESTool
- TEXT - These files are used for the original version of the DESTool

## Modules

- [ui](config/ui) - The directory contains the MainWindow extension of the Qt MainWindow. Our main window will include all of the widgets.
- [widgets](config/ui/widgets) - The directory contains the widgets to be displayed on screen.

## Execution

The installation of the module will also install the _ConfigCreator_ executable. Simply run the command:

```console
ConfigCreator
```

to run the program.

## Requirements

```console
Python3.6
PyQt5
QDarkStyle
```
