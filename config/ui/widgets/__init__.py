from PyQt5.QtWidgets import QListWidget, QGridLayout, QListWidgetItem, QWidget, QPushButton, QStyle, QCommonStyle
from PyQt5.QtWidgets import QComboBox, QLabel, QLineEdit
from PyQt5.QtGui import QDoubleValidator, QIntValidator
from PyQt5.QtCore import Qt


event_types = [('A', 'ARRIVAL'), ('D', 'DEPARTURE'), ('T', "TERMINATION")]
queue_types = ["FIFO", "LIFO"]


class EventTypeWidget(QWidget):

    def __init__(self, parent: QWidget = None) -> None:
        """

        """
        super().__init__(parent)

        grid = QGridLayout(self)

        self.uparrow = QPushButton()
        up_style = QCommonStyle()
        self.uparrow.clicked.connect(self.__up_clicked__)
        self.uparrow.setIcon(up_style.standardIcon(QStyle.SP_ArrowUp))

        self.downarrow = QPushButton()
        down_style = QCommonStyle()
        self.downarrow.clicked.connect(self.__down_clicked__)
        self.downarrow.setIcon(down_style.standardIcon(QStyle.SP_ArrowDown))

        self.event_list = QListWidget()
        for item in event_types:
            self.event_list.addItem(QListWidgetItem(item[1]))

        self.label = QLabel("Event Order", self)
        self.label.setAlignment(Qt.AlignCenter)

        grid.addWidget(self.label, 0, 0, 1, 5)
        grid.addWidget(self.event_list, 1, 0, 3, 5)
        grid.addWidget(self.uparrow, 7, 1, 1, 1)
        grid.addWidget(self.downarrow, 7, 3, 1, 1)

        self.setLayout(grid)

    def __up_clicked__(self) -> None:
        """
        The up button was pushed, so if we can, move the selected text up one
        :return: None
        """
        idx = self.event_list.currentRow()

        if idx > 0:
            curr_item = self.event_list.takeItem(idx)
            self.event_list.insertItem(idx - 1, curr_item)
            self.event_list.setCurrentRow(idx - 1)

    def __down_clicked__(self) -> None:
        """
        The down button was pushed, so if we can, move the selected text down one
        :return: None
        """
        idx = self.event_list.currentRow()

        if idx != self.event_list.count() - 1:
            curr_item = self.event_list.takeItem(idx)
            self.event_list.insertItem(idx + 1, curr_item)
            self.event_list.setCurrentRow(idx + 1)


class CreateEventWidget(QWidget):

    def __init__(self, parent: QWidget = None) -> None:
        """"

        """
        super().__init__(parent)

        grid = QGridLayout(self)

        self.event_list = QListWidget(self)

        self.combo_box = QComboBox(self)
        for x in event_types:
            self.combo_box.addItem(x[1])

        self.event_entry = QLineEdit(self)
        self.event_entry.setPlaceholderText("Event No.")
        self.event_entry.setValidator(QIntValidator(0, 10000))

        self.time = QLineEdit(self)
        self.time.setPlaceholderText("Event Time")
        self.time.setValidator(QDoubleValidator(0, 10000, 2))

        self.accept = QPushButton(self)
        accept_style = QCommonStyle()
        self.accept.clicked.connect(self.__add_event__)
        self.accept.setIcon(accept_style.standardIcon(QStyle.SP_DialogApplyButton))

        self.remove = QPushButton(self)
        remove_style = QCommonStyle()
        self.remove.clicked.connect(self.__remove_event__)
        self.remove.setIcon(remove_style.standardIcon(QStyle.SP_DialogCloseButton))

        grid.addWidget(self.event_entry, 0, 0, 1, 1)
        grid.addWidget(self.time, 1, 0, 1, 1)
        grid.addWidget(self.combo_box, 2, 0, 1, 1)
        grid.addWidget(self.event_list, 0, 1, 4, 4)
        grid.addWidget(self.accept, 4, 1, 1, 2)
        grid.addWidget(self.remove, 4, 3, 1, 2)

        self.setLayout(grid)

    def __add_event__(self) -> None:
        """
        Add an event to the list if there is a valid event number, time, and event type
        :return: None
        """
        if self.time.text() and self.event_entry.text():
            event_type = str(self.combo_box.currentText())
            time = self.time.text()
            event = "--" if event_type.startswith("T") or event_type.startswith("t") else self.event_entry.text()

            text = "{} {} {}".format(event, event_type, time)
            self.event_list.addItem(QListWidgetItem(text))

            self.time.clear()
            self.event_entry.clear()

    def __remove_event__(self) -> None:
        """
        Remove and event from the list where the current selected line of text resides
        :return: None
        """
        item = self.event_list.takeItem(self.event_list.currentRow())
        item = None


class QueueInfo(QWidget):

    def __init__(self, parent: QWidget = None) -> None:
        """

        """
        super().__init__(parent)
        self.queue_cap = QLineEdit(self)
        self.queue_cap.setPlaceholderText("Queue Capacity")
        self.queue_cap.setValidator(QIntValidator(0, 10000))

        self.queue_num = QLineEdit(self)
        self.queue_num.setPlaceholderText("No. of Queues")
        self.queue_num.setValidator(QIntValidator(0, 10000))

        self.combo_box = QComboBox(self)
        [self.combo_box.addItem(x) for x in queue_types]

        grid = QGridLayout(self)
        grid.addWidget(self.queue_cap, 0, 0, 1, 1)
        grid.addWidget(self.queue_num, 1, 0, 1, 1)
        grid.addWidget(self.combo_box, 2, 0, 1, 1)

        self.setLayout(grid)


class ServerInfo(QWidget):

    def __init__(self, parent: QWidget = None) -> None:
        """

        """
        super().__init__(parent)
        self.server_cap = QLineEdit(self)
        self.server_cap.setPlaceholderText("Server Capacity")
        self.server_cap.setValidator(QIntValidator(0, 10000))

        self.server_num = QLineEdit(self)
        self.server_num.setPlaceholderText("No. of Servers")
        self.server_num.setValidator(QIntValidator(0, 10000))

        grid = QGridLayout(self)
        grid.addWidget(self.server_cap, 0, 0, 1, 1)
        grid.addWidget(self.server_num, 1, 0, 1, 1)

        self.setLayout(grid)
