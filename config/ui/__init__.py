from PyQt5.QtWidgets import QMainWindow, QWidget, QGridLayout, QAction, QFileDialog, QListWidgetItem
from PyQt5.QtCore import QFile, QIODevice, QTextStream
import config.ui.widgets
from PyQt5.QtCore import Qt
import re
import json


class MainWindow(QMainWindow):

    def __init__(self, parent: QWidget = None) -> None:
        """

        """
        super().__init__(parent)

        self.filename = None

        central = QWidget(self)

        self.servers = widgets.ServerInfo(self)
        self.queues = widgets.QueueInfo(self)
        self.event_type = widgets.EventTypeWidget(self)
        self.create_event = widgets.CreateEventWidget(self)

        grid = QGridLayout()
        grid.addWidget(self.queues, 0, 0, 1, 1)
        grid.addWidget(self.servers, 1, 0, 1, 1)
        grid.addWidget(self.event_type, 2, 0, 3, 3)
        grid.addWidget(self.create_event, 0, 3, 6, 3)

        central.setLayout(grid)

        self.setCentralWidget(central)

        main_menu = self.menuBar()
        file_menu = main_menu.addMenu('File')

        load = QAction("Load Config File", self)
        load.setShortcut('Ctrl+O')
        load.triggered.connect(self.__load_config__)
        file_menu.addAction(load)

        save = QAction("Save", self)
        save.setShortcut('Ctrl+S')
        save.triggered.connect(self.__save__)
        file_menu.addAction(save)

        save_as = QAction("Save As", self)
        save_as.setShortcut('Ctrl+Shift+S')
        save_as.triggered.connect(self.__save_as__)
        file_menu.addAction(save_as)

        close = QAction("Close", self)
        close.setShortcut('Ctrl+Q')
        close.triggered.connect(self.close)
        file_menu.addAction(close)

        edit_menu = main_menu.addMenu("Edit")

        clear = QAction("Clear", self)
        clear.triggered.connect(self.__clear__)
        edit_menu.addAction(clear)

    def __clear__(self) -> None:
        """
        Clear all data from the widgets
        :return: None
        """
        self.servers.server_num.clear()
        self.servers.server_cap.clear()
        self.queues.queue_num.clear()
        self.queues.queue_cap.clear()
        self.create_event.event_list.clear()
        self.create_event.event_entry.clear()
        self.create_event.time.clear()

    def __load_config__(self) -> None:
        """
        Open a dialog to load in a file. After the file is read in it will
        be displayed on the screen and the user can save or edit this file
        :return: None
        """
        dlg = QFileDialog(self)
        dlg.setNameFilters(["Text files (*.txt)", "JSON files (*.json)"])

        if dlg.exec_():
            file = dlg.selectedFiles()[0]

            self.__clear__()

            if ".txt" in file:
                self.__parse_text_file__(file)
            elif ".json" in file:
                self.__parse_json_file__(file)

    def __save__(self) -> None:
        """
        Save the file with the same name that is currently loaded. If no name is saved, then call the save
        as function.
        :return: None
        """
        if self.filename:
            f = open(self.filename, 'w+')

            if ".txt" in self.filename:
                f.write(self.__create_txt__())
            else:
                f.write(self.__create_json__())

            f.close()
        else:
            self.__open_save__()

    def __save_as__(self) -> None:
        """
        Open a dialog to save the file as a certain name
        :return: None
        """
        self.__open_save__()

    def __open_save__(self) -> None:
        """
        Common function to open a dialog to save the data to
        :return:
        """
        file, _ = QFileDialog.getSaveFileName(self, "Save File", '.', "SIM (*.txt *.json)")

        """ make sure a true file is seleected """
        if file is None:
            return

        f = QFile(file)
        f.open(QIODevice.WriteOnly)

        out = QTextStream(f)

        if ".txt" in file:
            out << self.__create_txt__()
        elif ".json" in file:
            out << self.__create_json__()

        f.close()

    def __parse_text_file__(self, file: str) -> None:
        """
        Parse the text file and make sure that all of the data is added to the screen
        :param file: name of the file to be parsed
        :return: none
        """
        lines = []
        with open(file) as f:
            lines = [line.rstrip('\n') for line in f]

        if lines:
            set_queue_cap = False
            set_queue_num = False
            set_server_cap = False
            set_server_num = False
            set_queue_type = False
            set_event_order = False

            for line in lines:
                if re.search('queue capacity', line, re.IGNORECASE):
                    set_queue_cap = True
                    attributes = line.split()
                    self.queues.queue_cap.setText(attributes[len(attributes) - 1])
                elif re.search('number of queues', line, re.IGNORECASE):
                    set_queue_num = True
                    attributes = line.split()
                    self.queues.queue_num.setText(attributes[len(attributes) - 1])
                elif re.search('server capacity', line, re.IGNORECASE):
                    set_server_cap = True
                    attributes = line.split()
                    self.servers.server_cap.setText(attributes[len(attributes) - 1])
                elif re.search('number of servers', line, re.IGNORECASE):
                    set_server_num = True
                    attributes = line.split()
                    self.servers.server_num.setText(attributes[len(attributes) - 1])
                elif re.search('queue type', line, re.IGNORECASE):
                    set_queue_type = True
                    attributes = line.split()
                    queue_type = attributes[len(attributes) - 1]
                    index = self.queues.combo_box.findText(queue_type, Qt.MatchFixedString)
                    if index >= 0:
                        self.queues.combo_box.setCurrentIndex(index)
                elif re.search('event order', line, re.IGNORECASE):

                    set_event_order = True
                    fsplit = line.split(":")
                    event_types = []

                    if len(fsplit) == 2:
                        types = ''.join(fsplit[1].split()).split(',')

                        for type in types:
                            for gtype in widgets.event_types:
                                if type[:1] == gtype[0] and gtype[1] not in event_types:
                                    event_types.append(gtype[1])

                    """ Add in any missing values to the end of the list """
                    for gtype in widgets.event_types:
                        if gtype[1] not in event_types:
                            event_types.append(gtype[1])

                    if event_types:
                        """ remove all items before adding them back in the specified order """
                        self.event_type.event_list.clear()
                        for e in event_types:
                            self.event_type.event_list.addItem(QListWidgetItem(e))
                else:
                    sp = line.split()[1:]

                    if len(sp) == 3:
                        for gtype in widgets.event_types:
                            if sp[1][:1] == gtype[0]:
                                sp[1] = gtype[1]

                        text = ' '.join(sp)

                        self.create_event.event_list.addItem(QListWidgetItem(text))

            if set_event_order and set_queue_type and set_queue_num and set_queue_cap and set_server_num and set_server_cap:
                self.filename = file

    def __parse_json_file__(self, file: str) -> None:
        """
        Parse the json information that was read in and display the information on screen
        :param file: name of the json file
        :return: none
        """
        if not file:
            return

        my_json = {}

        with open(file, 'r') as f:
            my_json = json.load(f)

        if "EVENTS" in my_json:
            for event in my_json["EVENTS"]:
                e = event["ENTITY"] if "ENTITY" in event else "--"
                etype = event["TYPE"] if "TYPE" in event else None
                time = event["TIME"] if "TIME" in event else None

                for gtype in widgets.event_types:
                    if etype[:1] == gtype[0]:
                        etype = gtype[1]

                text = "{} {} {}".format(e, etype, time)
                self.create_event.event_list.addItem(QListWidgetItem(text))

        if "EVENT_ORDER" in my_json:
            types = []
            for type in my_json["EVENT_ORDER"]:

                for gtype in widgets.event_types:
                    if type[:1] == gtype[0] and gtype[1] not in types:
                        types.append(gtype[1])

            """ Add any missing items to the ed of the list """
            for gytpe in widgets.event_types:
                if gtype[1] not in types:
                    types.append(gtype[1])

            if types:
                self.event_type.event_list.clear()
                for e in types:
                    self.event_type.event_list.addItem(QListWidgetItem(e))

        if "QUEUES" in my_json:
            id = 0
            capacity = 0
            qtype = "FIFO"

            for queue in my_json["QUEUES"]:
                id = max(id, int(queue["ID"])) if "ID" in queue else 0
                capacity = max(capacity, int(queue["CAPACITY"])) if "CAPACITY" in queue else 0
                qtype = queue["TYPE"] if "TYPE" in queue else qtype

            self.queues.queue_num.setText(str(id))
            self.queues.queue_cap.setText(str(capacity))

            index = self.queues.combo_box.findText(qtype, Qt.MatchFixedString)
            if index >= 0:
                self.queues.combo_box.setCurrentIndex(index)

        if "SERVERS" in my_json:
            id = 0
            capacity = 0

            for server in my_json["SERVERS"]:
                id = max(id, int(server["ID"])) if "ID" in server else 0
                capacity = max(capacity, int(server["CAPACITY"])) if "CAPACITY" in server else 0

            self.servers.server_num.setText(str(id))
            self.servers.server_cap.setText(str(capacity))

        self.filename = file

    def __create_txt__(self) -> str:
        """
        Create a text file string for all information on the screen
        :return: string for the file
        """
        lines = []
        lines.append("Queue Capacity: " + self.queues.queue_cap.text())
        lines.append("Number of Queues: " + self.queues.queue_num.text())
        lines.append("Server Capacity: " + self.servers.server_cap.text())
        lines.append("Number of Servers: " + self.servers.server_num.text())
        lines.append("Queue Type: " + self.queues.combo_box.currentText())
        order = [str(self.event_type.event_list.item(i).text()) for i in range(self.event_type.event_list.count())]
        lines.append("Event Order: " + ', '.join(order))
        [lines.append(str(i) + " " + str(self.create_event.event_list.item(i).text())) for i in range(self.create_event.event_list.count())]

        return '\n'.join(lines)

    def __create_json__(self) -> str:
        """
        Create a json file string for all information on the screen
        :return: string for the file
        """
        my_json = {}

        my_json["QUEUES"] = []
        for x in range(int(self.queues.queue_num.text())):
            my_json["QUEUES"].append({
                "ID": str(x+1),
                "CAPACITY": self.queues.queue_cap.text(),
                "TYPE": self.queues.combo_box.currentText()
            })

        my_json["SERVERS"] = []
        for x in range(int(self.servers.server_num.text())):
            my_json["SERVERS"].append({
                "ID": str(x + 1),
                "CAPACITY": self.servers.server_cap.text()
            })

        my_json["EVENT_ORDER"] = [str(self.event_type.event_list.item(i).text()) for i in range(self.event_type.event_list.count())]

        my_json["EVENTS"] = []
        for i in range(self.create_event.event_list.count()):
            txt = str(self.create_event.event_list.item(i).text())
            sp = txt.split()

            if len(sp) == 3:
                entity = sp[0]
                etype = sp[1]
                time = sp[2]

                if '--' in entity:
                    my_json["EVENTS"].append({"TYPE": etype, "TIME": time})
                else:
                    my_json["EVENTS"].append({"ENTITY": entity, "TYPE": etype, "TIME": time})

        return json.dumps(my_json, indent=4, sort_keys=True)